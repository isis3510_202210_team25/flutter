import 'package:cloud_firestore/cloud_firestore.dart';
import '../utilities/database_singleton.dart';
import '../models/device.dart';

DatabaseSingleton db = DatabaseSingleton();
FirebaseFirestore firestore = db.getFirestore();

class DeviceController {
  Future<List<Device>> findAll() async {
    QuerySnapshot<Map<String, dynamic>> snapshot =
        await firestore.collection('devices').get();

    return snapshot.docs
        .map((docSnapshot) => Device.fromDocumentSnapshot(docSnapshot))
        .toList();
  }
}
