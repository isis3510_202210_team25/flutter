import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:collection/collection.dart';
import 'package:freeazy/controllers/product_controller.dart';
import 'package:freeazy/models/extended_product.dart';
import '../utilities/database_singleton.dart';
import '../models/cluster.dart';
import '../models/product.dart';

DatabaseSingleton db = DatabaseSingleton();
FirebaseFirestore firestore = db.getFirestore();

class ClusterController {
  addCluster(Cluster clusterData) async {
    await firestore.collection('clusters').add(clusterData.toMap());
  }

  Future<List<Cluster>> findAll() async {
    QuerySnapshot<Map<String, dynamic>> snapshot =
        await firestore.collection('clusters').get();

    return snapshot.docs
        .map((docSnapshot) => Cluster.fromDocumentSnapshot(docSnapshot))
        .toList();
  }

  Future<List<Cluster>> findAllByDevice(String deviceId) async {
    QuerySnapshot<Map<String, dynamic>> snapshot = await firestore
        .collection('clusters')
        .where('deviceId', isEqualTo: deviceId)
        .get();

    return snapshot.docs
        .map((docSnapshot) => Cluster.fromDocumentSnapshot(docSnapshot))
        .toList();
  }

  Future<List<Cluster>> findAllByProduct(String? productId) async {
    QuerySnapshot<Map<String, dynamic>> snapshot = await firestore
        .collection('clusters')
        .where('productId', isEqualTo: productId)
        .get();

    return snapshot.docs
        .map((docSnapshot) => Cluster.fromDocumentSnapshot(docSnapshot))
        .toList();
  }

  Future<ExtendedProduct> findOneExtended(Product product) async {
    // Get all clusters pertaining to that productId
    List<Cluster> clusters = await findAllByProduct(product.id);
    // ProductController productController = ProductController();
    // Product product = await productController.findOne(productId);

    ExtendedProduct extendedProduct = ExtendedProduct(clusters, product);
    return extendedProduct;
  }

  Future<List<ExtendedProduct>> findAllExtended(String deviceId) async {
    List<Cluster> clusters = await findAllByDevice(deviceId);

    Map<String, List<Cluster>> groups = groupBy(clusters, (Cluster c) {
      return c.productId;
    });

    List<ExtendedProduct> extendedProducts = [];
    ProductController productController = ProductController();

    for (var key in groups.keys) {
      Product product = await productController.findOne(key);
      extendedProducts.add(ExtendedProduct(groups[key], product));
    }

    return extendedProducts;
  }
}
