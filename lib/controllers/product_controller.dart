import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freeazy/models/product.dart';
import '../utilities/database_singleton.dart';

DatabaseSingleton db = DatabaseSingleton();
FirebaseFirestore firestore = db.getFirestore();

class ProductController {
  Future<List<Product>> findAll() async {
    QuerySnapshot<Map<String, dynamic>> snapshot =
        await firestore.collection('products').get();

    return snapshot.docs
        .map((docSnapshot) => Product.fromDocumentSnapshot(docSnapshot))
        .toList();
  }

  Future<Product> findOne(String id) async {
    DocumentSnapshot<Map<String, dynamic>> snapshot =
        await firestore.collection('products').doc(id).get();

    return Product.fromDocumentSnapshot(snapshot);
  }
}
