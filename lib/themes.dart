import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

CustomTheme currentTheme = CustomTheme();

class CustomTheme {
  static const darkAquaGreen = Color(0xFF182628);
  static const lightAquaGreen = Color(0xFF65CCB8);
  static const middleGreen = Color(0xFF57BA98);
  static const simpleGreen = Color(0xFF3B9453);
  static const lightGray = Color(0xFFF2F2F2);

  final defaultTheme = ThemeData(
    // Brightness
    brightness: Brightness.light,

    // Defining colors and themes
    primaryColor: simpleGreen,
    secondaryHeaderColor: lightAquaGreen,
    backgroundColor: lightGray,
    scaffoldBackgroundColor: lightGray,
    splashColor: middleGreen,

    colorScheme: const ColorScheme(
        background: lightGray,
        primary: simpleGreen,
        secondary: middleGreen,
        tertiary: lightAquaGreen,
        surface: lightGray,
        brightness: Brightness.light,
        onPrimary: simpleGreen,
        onBackground: lightGray,
        onSecondary: middleGreen,
        onSurface: lightGray,
        onError: Colors.red,
        error: Colors.red),

    // Defining font family
    fontFamily: 'Roboto',

    textTheme: GoogleFonts.ralewayTextTheme().copyWith(
        headline1:
            const TextStyle(color: darkAquaGreen, fontWeight: FontWeight.bold),
        headline2:
            const TextStyle(color: darkAquaGreen, fontWeight: FontWeight.bold),
        bodyText1: const TextStyle(color: darkAquaGreen, fontFamily: 'Roboto'),
        bodyText2: const TextStyle(color: middleGreen, fontFamily: 'Roboto')),
  );
}
