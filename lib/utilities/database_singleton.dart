import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseSingleton {
  static final DatabaseSingleton _instance = DatabaseSingleton._internal();
  static late FirebaseFirestore _firestore;

  FirebaseFirestore getFirestore() {
    return _firestore;
  }

  DatabaseSingleton._internal() {
    _firestore = FirebaseFirestore.instance;
  }

  factory DatabaseSingleton() {
    return _instance;
  }
}
