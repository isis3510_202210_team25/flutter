import 'package:flutter/material.dart';
import 'package:freeazy/models/cluster.dart';

class ClusterList extends StatelessWidget {
  const ClusterList({Key? key, required this.clusters}) : super(key: key);

  final List<Cluster> clusters;

  Widget _buildRow(Cluster cluster) {
    Color color;
    if (DateTime.now().isAfter(cluster.bestBefore)) {
      color = Colors.red;
    } else if (DateTime.now()
            .add(const Duration(days: 4))
            .isAfter(cluster.bestBefore) ||
        DateTime.now()
            .add(const Duration(days: 4))
            .isAtSameMomentAs(cluster.bestBefore)) {
      color = Colors.yellow;
    } else {
      color = Colors.green;
    }

    return Card(
        color: color,
        child: ListTile(
          leading: const Icon(Icons.food_bank_rounded),
          title: Text(cluster.label),
          subtitle: Text(
              'Days since added: ${DateTime.now().difference(cluster.addedAt).inDays}.'),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      padding: const EdgeInsets.all(8),
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemCount: clusters.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildRow(clusters[index]);
      },
    );
  }
}
