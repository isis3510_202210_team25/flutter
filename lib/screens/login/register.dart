import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterUserScreen extends StatefulWidget {
  const RegisterUserScreen({Key? key}) : super(key: key);

  @override
  State<RegisterUserScreen> createState() => _RegisterUserScreenState();
}

class _RegisterUserScreenState extends State<RegisterUserScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future signIn() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: emailController.text.trim(),
              password: passwordController.text.trim());

      print(userCredential);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {}
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color.fromARGB(255, 242, 242, 242),
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 300,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/login.png',
                  fit: BoxFit.cover,
                  height: 100,
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 32,
                ),
                child: Column(
                  children: [
                    const Text('Freeazy',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 40)),

                    TextFormField(
                      controller: emailController,
                      decoration: const InputDecoration(
                        //icon: Icon(Icons.email),

                        // enabledBorder: UnderlineInputBorder(
                        //     borderSide: BorderSide(
                        //         color: Color.fromARGB(255, 59, 148, 95))),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 59, 148, 95))),
                        hintText: 'Ingresa tu correo',
                        labelText: 'Correo',
                      ),
                      onChanged: (value) {
                        setState(() {});
                      },
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(color: Colors.grey),
                          //icon: Icon(Icons.lock),
                          // enabledBorder: UnderlineInputBorder(
                          //     borderSide: BorderSide(
                          //         color: Color.fromARGB(255, 59, 148, 95))),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromARGB(255, 59, 148, 95))),
                          hintText: 'Ingresa tu contraseña',
                          labelText: 'Contraseña',
                          floatingLabelStyle: TextStyle(color: Colors.green)),
                      onChanged: (value) {
                        setState(() {});
                      },
                    ),
                    const SizedBox(
                      height: 90,
                    ),
                    TextButton(
                      onPressed: signIn,
                      //icon: const Icon(Icons.login),
                      child: Container(
                        alignment: Alignment.center,
                        width: 150,
                        height: 35,
                        child: const Text(
                          'Registrar usuario',
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 59, 148, 94),
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ),
                    // Row(
                    //   children: [
                    //     const Text('Don\'t have an account?'),
                    //     TextButton(
                    //       onPressed: (() {
                    //         debugPrint("Welcome back!");
                    //         // Navigator.pushNamed(
                    //         //   context,
                    //         //   MyRoutes.signUp,
                    //         // );
                    //       }),
                    //       child: const Text(
                    //         'Sign Up',
                    //         style: TextStyle(
                    //           fontSize: 15,
                    //           fontWeight: FontWeight.bold,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    // ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
