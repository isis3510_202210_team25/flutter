import 'dart:developer';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:freeazy/screens/device/device_list.dart';
import 'package:freeazy/screens/login/register.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future signIn() async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: emailController.text.trim(),
              password: passwordController.text.trim());

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const DeviceList()));
    } on FirebaseAuthException catch (e) {
      if (e.code == 'wrong-password') {}
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: const Color.fromARGB(255, 242, 242, 242),
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 300,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                alignment: Alignment.center,
                child: Image.asset(
                  'assets/images/login.png',
                  fit: BoxFit.cover,
                  height: 100,
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 0,
                  horizontal: 32,
                ),
                child: Column(
                  children: [
                    const Text('Freeazy',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 40)),
                    TextFormField(
                      controller: emailController,
                      decoration: const InputDecoration(
                        //icon: Icon(Icons.email),

                        // enabledBorder: UnderlineInputBorder(
                        //     borderSide: BorderSide(
                        //         color: Color.fromARGB(255, 59, 148, 95))),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 59, 148, 95))),
                        hintText: 'Enter your e-mail',
                        labelText: 'E-mail',
                      ),
                      onChanged: (value) {
                        setState(() {});
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                          labelStyle: TextStyle(color: Colors.grey),
                          //icon: Icon(Icons.lock),
                          // enabledBorder: UnderlineInputBorder(
                          //     borderSide: BorderSide(
                          //         color: Color.fromARGB(255, 59, 148, 95))),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromARGB(255, 59, 148, 95))),
                          hintText: 'Enter your password',
                          labelText: 'Password',
                          floatingLabelStyle: TextStyle(color: Colors.green)),
                      onChanged: (value) {
                        setState(() {});
                      },
                    ),
                    const SizedBox(
                      height: 80,
                    ),

                    TextButton(
                      onPressed: signIn,
                      //icon: const Icon(Icons.login),
                      child: Container(
                        alignment: Alignment.center,
                        width: 150,
                        height: 35,
                        child: const Text(
                          'Log in',
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 59, 148, 94),
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                          textStyle: const TextStyle(fontSize: 18),
                          alignment: Alignment.centerLeft,
                          primary: const Color.fromARGB(255, 59, 148, 95)),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    const RegisterUserScreen()));
                        //Navigator.pushNamed(context, MyRoutes.forgotPassword);
                      },
                      child: const Text(
                        'You don\' have an account yet?',
                      ),
                    ),
                    // Row(
                    //   children: [
                    //     const Text('Don\'t have an account?'),
                    //     TextButton(
                    //       onPressed: (() {
                    //         debugPrint("Welcome back!");
                    //         // Navigator.pushNamed(
                    //         //   context,
                    //         //   MyRoutes.signUp,
                    //         // );
                    //       }),
                    //       child: const Text(
                    //         'Sign Up',
                    //         style: TextStyle(
                    //           fontSize: 15,
                    //           fontWeight: FontWeight.bold,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    //   mainAxisAlignment: MainAxisAlignment.center,
                    // ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
