import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:freeazy/controllers/device_controller.dart';
import 'package:freeazy/models/device.dart';
import 'package:freeazy/screens/product/user_product_list.dart';

class DeviceList extends StatefulWidget {
  const DeviceList({Key? key}) : super(key: key);

  @override
  DevicesState createState() => DevicesState();
}

class DevicesState extends State<DeviceList> {
  Future<List<Device>>? _futureDevices;
  final DeviceController _deviceController = DeviceController();

  @override
  void initState() {
    super.initState();

    _futureDevices = _deviceController.findAll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Your devices'),
          backgroundColor: const Color(0xFF3B9453),
        ),
        body: Center(
          child: FutureBuilder<List<Device>>(
            future: _futureDevices,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Device>? data = snapshot.data;
                return _buildList(data);
              } else if (snapshot.hasError) {
                String error = 'Error: ${snapshot.error.toString()}';
                log(error);
                return Text(error);
              } else {
                return const CircularProgressIndicator();
              }
            },
          ),
        ));
  }

  Widget _buildList(List<Device>? deviceList) {
    return ListView.builder(
      itemCount: deviceList?.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildRow(deviceList![index]);
      },
    );
  }

  Widget _buildRow(Device device) {
    return Card(
        child: ListTile(
      leading: Image.network(device.image),
      title: Text(device.label),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    const UserProductList(deviceId: '2tLBVgjRxCscgwNYwwQP')));
      },
    ));
  }
}
