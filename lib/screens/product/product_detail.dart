import 'package:flutter/material.dart';
import 'package:freeazy/models/extended_product.dart';
import 'package:freeazy/screens/cluster/cluster_list.dart';
import 'package:google_fonts/google_fonts.dart';

class ProductDetail extends StatelessWidget {
  const ProductDetail({Key? key, required this.extended}) : super(key: key);

  final ExtendedProduct extended;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF3B9453),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            _hero(),
            _label(),
            _description(),
            ClusterList(clusters: extended.getClusters()!),
          ],
        ),
      ),
    );
  }

  Widget _hero() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Image.network(extended.product.image));
  }

  Widget _label() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(extended.product.name,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'RobotoMono',
                  fontSize: 24)),
        ],
      ),
      alignment: Alignment.center,
    );
  }

  Widget _description() {
    return Column(children: <Widget>[
      _attributeRow('Description', extended.product.description),
      _attributeRow('Good practices', extended.product.bestPractices),
    ]);
  }

  Widget _attributeRow(String name, String corpus) {
    return Container(
      child: Column(children: <Widget>[
        Text(
          name,
          style: GoogleFonts.raleway(fontSize: 16),
          textAlign: TextAlign.left,
        ),
        Text(
          corpus,
          style: GoogleFonts.roboto(fontSize: 12),
          textAlign: TextAlign.left,
        ),
      ]),
      padding: const EdgeInsets.all(16),
      alignment: Alignment.centerLeft,
    );
  }
}
