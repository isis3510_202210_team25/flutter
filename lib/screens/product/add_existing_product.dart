import 'package:flutter/material.dart';
import 'package:freeazy/controllers/cluster_controller.dart';
import 'package:freeazy/models/product.dart';
import '../../controllers/product_controller.dart';
import 'dart:developer';
import 'package:freeazy/models/cluster.dart';

class ExisitingProductList extends StatefulWidget {
  const ExisitingProductList({Key? key}) : super(key: key);

  @override
  ExistingProductState createState() => ExistingProductState();
}

class ExistingProductState extends State<ExisitingProductList> {
  ProductController controller = ProductController();
  Future<List<Product>>? futureProducts;

  @override
  void initState() {
    super.initState();
    futureProducts = controller.findAll();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Existing products'),
          backgroundColor: const Color(0xFF3B9453),
        ),
        body: Center(
          child: FutureBuilder<List<Product>>(
            future: futureProducts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<Product>? productList = snapshot.data;
                return _buildList(productList);
              } else if (snapshot.hasError) {
                String error = 'Error: ${snapshot.error.toString()}';
                log(error);
                return Text(error);
              } else {
                return const CircularProgressIndicator();
              }
            },
          ),
        ));
  }

  Widget _buildList(List<Product>? productList) {
    return ListView.builder(
      itemCount: productList?.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildRow(productList![index]);
      },
    );
  }

  Widget _buildRow(Product? product) {
    return Card(
      child: ListTile(
        leading: Image.network(product!.image),
        title: Text(product.name),
        trailing: IconButton(
          icon: const Icon(Icons.add),
          onPressed: () {
            // Create cluster without id because the Firestore manager automatically handles it
            log('On pressed function entered');
            // This values are hardcoded **for now**
            const String userRef = '35kqnyEuK3y3IH1igaXT';
            const String deviceRef = '2tLBVgjRxCscgwNYwwQP';
            Cluster clusterData = Cluster(
              addedAt: DateTime.now(),
              bestBefore: DateTime(2022, 10, 31),
              deviceId: deviceRef,
              label: 'Default',
              productId: userRef,
              //stock: 1
            );

            ClusterController clusterController = ClusterController();
            clusterController.addCluster(clusterData);
          },
        ),
      ),
    );
  }
}
