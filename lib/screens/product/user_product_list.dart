import 'package:flutter/material.dart';
import 'package:freeazy/screens/product/add_existing_product.dart';
import 'package:freeazy/controllers/cluster_controller.dart';
import 'package:freeazy/models/extended_product.dart';
import 'package:freeazy/screens/product/product_detail.dart';
import 'package:freeazy/calendar.dart';

class UserProductList extends StatefulWidget {
  const UserProductList({Key? key, required this.deviceId}) : super(key: key);

  final String deviceId;

  @override
  UserProductsState createState() => UserProductsState();
}

class UserProductsState extends State<UserProductList> {
  // What the actual fuck
  Future<List<ExtendedProduct>>? _extendedProducts;
  ClusterController clusterController = ClusterController();

  @override
  void initState() {
    super.initState();

    _extendedProducts = clusterController.findAllExtended(widget.deviceId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your products'),
        backgroundColor: const Color(0xFF3B9453),
      ),
      body: FutureBuilder<List<ExtendedProduct>>(
        future: _extendedProducts,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<ExtendedProduct>? data = snapshot.data;
            return _buildList(data);
          } else if (snapshot.hasError) {
            return const Text('FAILURE');
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        backgroundColor: Colors.green,
        foregroundColor: Colors.white,
        onPressed: () => {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const ExisitingProductList())),
        },
      ),
    );
  }

  Widget _buildList(data) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return _buildRow(data[index]);
      },
    );
  }

  Widget _buildRow(ExtendedProduct extended) {
    CalendarFacade calendarFacade = CalendarFacade();

    return Card(
      child: ListTile(
        leading: Image.network(extended.product.image),
        title: Text(extended.product.name),
        trailing: IconButton(
          icon: const Icon(Icons.event),
          onPressed: () {
            calendarFacade.insert(extended.product.name, DateTime(2022, 03, 27),
                DateTime(2022, 03, 27));
          },
        ),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ProductDetail(
                        extended: extended,
                      )));
        },
      ),
    );
  }
}
