import 'package:cloud_firestore/cloud_firestore.dart';

class Cluster {
  final String? id;
  final String label;
  //final int stock;
  final DateTime addedAt;
  final DateTime bestBefore;
  final String productId;
  final String deviceId;

  Cluster(
      {this.id,
      required this.label,
      //required this.stock,
      required this.addedAt,
      required this.bestBefore,
      required this.productId,
      required this.deviceId});

  Map<String, dynamic> toMap() {
    return {
      'label': label,
      //'stock': stock,
      'addedAt': Timestamp.fromDate(addedAt),
      'bestBefore': Timestamp.fromDate(bestBefore),
      'productId': productId,
      'deviceId': deviceId,
    };
  }

  Cluster.fromDocumentSnapshot(DocumentSnapshot<Map<String, dynamic>> doc)
      : id = doc.id,
        label = doc.data()!['label'],
        // stock = int.parse(doc.data()!['stock']),
        addedAt = DateTime.parse(doc.data()!['addedAt'].toDate().toString()),
        bestBefore =
            DateTime.parse(doc.data()!['bestBefore'].toDate().toString()),
        productId = doc.data()!['productId'],
        deviceId = doc.data()!['deviceId'];
}
