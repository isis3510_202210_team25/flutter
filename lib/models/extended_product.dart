/*
This class incorporates the best of two worlds. It can be
seen as a BRIDGE between cluster and product.
*/

import 'package:freeazy/models/cluster.dart';
import 'package:freeazy/models/product.dart';

class ExtendedProduct {
  // Cluster inheritance
  final List<Cluster>? clusters;

  // Product inheritance
  final Product product;

  ExtendedProduct(this.clusters, this.product);

  List<Cluster>? getClusters() {
    return clusters;
  }

  Product getProduct() {
    return product;
  }
}
