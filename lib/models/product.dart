import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  final String? id;
  final String name;
  final String description;
  final String bestPractices;
  final String image;

  Product(
      {this.id,
      required this.name,
      required this.description,
      required this.bestPractices,
      required this.image});

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'description': description,
      'bestPractices': bestPractices,
      'image': image
    };
  }

  Product.fromDocumentSnapshot(DocumentSnapshot<Map<String, dynamic>> doc)
      : id = doc.id,
        name = doc.data()!['name'],
        description = doc.data()!['description'],
        bestPractices = doc.data()!['bestPractices'],
        image = doc.data()!['image'];
}
