import 'package:cloud_firestore/cloud_firestore.dart';

class Device {
  final String? id;
  final int capacity;
  final String label;
  final String image;
  final String userId;
  final String brandId;

  Device(
      {this.id,
      required this.capacity,
      required this.label,
      required this.image,
      required this.userId,
      required this.brandId});

  Map<String, dynamic> toMap() {
    return {
      'capacity': capacity,
      'label': label,
      'image': image,
      'userId': userId,
      'brandId': brandId
    };
  }

  Device.fromDocumentSnapshot(DocumentSnapshot<Map<String, dynamic>> doc)
      : id = doc.id,
        capacity = doc.data()!['capacity'],
        label = doc.data()!['label'],
        image = doc.data()!['image'],
        userId = doc.data()!['userId'],
        brandId = doc.data()!['brandId'];
}
